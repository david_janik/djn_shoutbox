Aufgabenstellung:
Entwickle einen funktionalen Prototypen für eine webbasierte Shoutbox. Zur Konkretisierung erhältst du folgende Anforderungen:

Es ist ein One-Pager. Am Seitenkopf wird eine Frage gestellt. Über ein Formular kann diese Frage beantwortet werden. Die Antworten auf die Frage werden im unteren Teil der Seite aufgelistet.
Umsetzung in PHP mit einem Framework deiner Wahl. Stelle uns deine Lösung als Git-Repository zur Verfügung.

User Stories:
Als anonymer Nutzer gebe ich meine Antwort zu einer Frage ab, um meine Idee in die Diskussion einzubringen.
Als anonymer Nutzer lese ich die Antworten anderer Nutzer, um mir einen Überblick über vorhandene Ideen zu verschaffen.
Als Online-Moderation überprüfe ich eingehende Antworten, um sie bei Regelverstößen zu sperren.


Fragestellung:
Wie bewertest du die oben gestellten Anforderungen?
    - für diese Anforderungn ein cms zu bemühen ist fast schon zu viel, aber es geht immernoch schneller als die funktionalität komplett neu zu schreiben.

Welche Informationen fehlen dir?
    - Welche Informationen / Benutzerdaten sollen für jede Antwort eigeholt werden?
    - Ist eine Filterung der Antworten gewünscht? nach Themen / Datum / 
    - sollen eMailadressen der Antwortenden Nutzer bestätigt werden?
    - Welcher Ansatz für Vermeidung von spam ist gewünscht? Honeypot / captcha / anderes?
    - wie soll die Moderation aussehen? - Freigabe nach Überprüfung / information über eingehende Antworten mit der Möglickeit sie im Nachhinein zu speren?

Erläutere uns deinen Ansatz zur Umsetzung des Prototypen
    - Die Funktionalität lässt sich mit Drupal Boardmitteln abdecken.
    - Ein node type 'shoutbox' besteht aus Überschrift / Bild / Textfeld
    - Die funktionalität die für die Antworten benötigt wird deckt das Kommentarsystem ab, inclusive Eingabeformular
    - Darstellung der Antworten für die Online-Moderation mit views ist ebenfalls schon gegeben unter: ./admin/content/comment.
    - ich habe Drupal 8 benutzt, weil ich zum einen in Letzter Zeit recht viel damit fgearbeitet habe, zum Andren ist es relativ wenig aufwändig vernünftiges Markup zu bekommen.
    - Ich habe mich auf die Funktionalität konzentriert und keine aufwändiges Styling betrieben - aber versucht möglichst suberes markup zu erzeugen.
    
    